<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Units;
use App\User;
use App\ActivityLog;
use Maatwebsite\Excel\Facades\Excel;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function sign_in(Request $request)
    {
        $check = DB::table('users')->where('email', $request['email'])->exists();
        $res = new \stdClass;
        if (!$check) {
            $res->success = false;
            $res->message = "User doesn't exist";
            return json_encode($res);
        } else {
            $user = DB::table('users')
                    ->where('email', $request['email'])
                    ->where('password', $request['password'])
                    ->first();
            if ($user) {
                $res->success = true;
                $res->message = "";
                $res->data = $user;
                return json_encode($res);
            } else {
                $res->success = false;
                $res->message = "Password is invalid";
                return json_encode($res);
            }
        }
    }
    public function sign_up(Request $request)
    {
        $check = DB::table('users')->where('email', $request['email'])->exists();
        $res = new \stdClass;
        if ($check) {
            $res->success = false;
            $res->message = "User already exists!";
        } else {
            $user = New User;
            $user->email = $request['email'];
            $user->password = $request['password'];
            $user->name = $request['name'];
            $user->usertype = '0';
            $user->save();

            $res->success = true;
            $res->message = "";
        }
        return json_encode($res);
    }
    public function get_buildings()
    {
        $buildings = DB::select(DB::raw("SELECT * FROM `buildings`"));
        
        return $buildings;
    }

    public function get_rooms(Request $request)
    {
        $rooms = DB::select(DB::raw("SELECT * FROM `rooms`"));
        
        return $rooms;
    }

    public function add_unit(Request $request)
    {
        $res = new \stdClass;
        $isUnit = DB::table('units')
                    ->where('unit_num', $request['unit_num'])
                    ->where('room', $request['room'])
                    ->where('position', $request['position'])
                    ->where('location', $request['location'])
                    ->where('building', $request['buildingid'])
                    ->exists();
        if($isUnit == '1'){
            $res->success = false;
            $res->message = "This unit already exists!";
        } else {
            $units = New Units;
            $units->unit_num = $request['unit_num'];
            $units->room = $request['room'];
            $units->position = $request['position'];
            $units->location = $request['location'];
            $units->notes = $request['notes'];
            $units->building = $request['buildingid'];
            $units->width = $request['width'];
            $units->height = $request['height'];
            $units->thickness = $request['thickness'];
            $units->horizontal_girds = $request['horizontal_girds'];
            $units->vertical_grids = $request['vertical_grids'];
            $units->repair_type = $request['repair_type'];
            if($request['width'] != null || $request['height'] != null || $request['thickness'] != null || $request['horizontal_girds'] != null || $request['vertical_grids'] != null || $request['repair_type'] != null)
                $units->status = '1';
            else
                $units->status = '0';
            
            $units->save();

            $added_unit = DB::table('units')
                    ->latest()
                    ->first();
            $added_unit->created_at = $this->datetimeformat($added_unit->created_at);
            $added_unit->updated_at = $this->datetimeformat($added_unit->updated_at);
            $unitids = array();
            array_push($unitids, $added_unit->id);
            $this->save_logs($unitids, 'new entry', $request->username);
            $res->success = true;
            $res->message = "";
            $res->data = $added_unit;
        }
        return json_encode($res);
    }

    public function update_unit(Request $request)
    {
        $res = new \stdClass;
        $units = Units::find($request->id);
        $units->unit_num = $request['unit_num'];
        $units->room = $request['room'];
        $units->position = $request['position'];
        $units->location = $request['location'];
        $units->notes = $request['notes'];
        $units->width = $request['width'];
        $units->height = $request['height'];
        $units->thickness = $request['thickness'];
        $units->horizontal_girds = $request['horizontal_girds'];
        $units->vertical_grids = $request['vertical_grids'];
        $units->repair_type = $request['repair_type'];
        if($units->status == '0'){
            if($request['width'] != null || $request['height'] != null || $request['thickness'] != null || $request['horizontal_girds'] != null || $request['vertical_grids'] != null || $request['repair_type'] != null){
                $units->status = '1';
            }else{
                $units->status = '0';
            }
        }
        $units->save();

        $updated_unit = DB::table('units')->where('id', $request->id)->first();
        
        $updated_unit->created_at = $this->datetimeformat($updated_unit->created_at);
        $updated_unit->updated_at = $this->datetimeformat($updated_unit->updated_at);   
        if (isset($updated_unit->schedule_for))
            $updated_unit->schedule_for = $this->dateformat($updated_unit->schedule_for);

        $unitids = array();
        array_push($unitids, $request->id);

        $res->success = true;
        $res->message = "";
        $res->data = $updated_unit;

        $this->save_logs($unitids, 'updated entry', $request->username);
        return json_encode($res);
    }

    public function get_units(Request $request)
    {
        $res = new \stdClass;
        $building = DB::table('buildings')->where('id', '=', $request->building_id)->first();
        $units = DB::select(DB::raw("SELECT a.*, l.name AS lname, p.name AS pname, r.name AS rname FROM `units` AS a 
                                    INNER JOIN `locations` AS l ON a.location = l.id
                                    INNER JOIN `rooms` AS r ON a.room = r.id
                                    INNER JOIN `positions` AS p ON a.position = p.id 
                                    WHERE a.building = '$request->building_id' AND a.status != '6' ORDER BY created_at DESC"));
        foreach($units as $unit){
            $unit->created_at = $this->datetimeformat($unit->created_at);
            $unit->updated_at = $this->datetimeformat($unit->updated_at);
            if (isset($unit->schedule_for))
                $unit->schedule_for = $this->dateformat($unit->schedule_for);
        }
        $res->success = true;
        $res->data = $units;
        return json_encode($res);
    }

    public function get_scheduled_units(Request $request)
    {
        $res = new \stdClass;
        $units = DB::select(DB::raw("SELECT a.*, l.name AS lname, p.name AS pname, r.name AS rname, b.buildingname FROM `units` AS a 
                                    INNER JOIN `buildings` AS b ON a.building = b.id 
                                    INNER JOIN `locations` AS l ON a.location = l.id
                                    INNER JOIN `rooms` AS r ON a.room = r.id
                                    INNER JOIN `positions` AS p ON a.position = p.id 
                                    WHERE a.status = '4' ORDER BY schedule_for, building"));
        foreach($units as $unit){
            $unit->measurements = $unit->thickness.' -- '.$unit->width.' x '.$unit->height;
            $unit->created_at = $this->datetimeformat($unit->created_at);
            $unit->updated_at = $this->datetimeformat($unit->updated_at);
            if (isset($unit->schedule_for))
                $unit->schedule_for = $this->dateformat($unit->schedule_for);
        }
        $res->success = true;
        $res->data = $units;
        return json_encode($res);
    }

    public function cut_sheet(Request $request)
    {
        $files = glob(public_path()."/excel/*"); // get all file names
        foreach($files as $file){ // iterate files
        if(is_file($file))
            unlink($file); // delete file
        }
        /* set status to 2*/
        $unit_ids = explode(",", $request->unit_ids);
        $building_id = $request->building_id;
        $sql = "UPDATE `units` SET STATUS = '2' WHERE ";
        foreach($unit_ids as $id){
            $sql .= "id = $id or ";
        }
        $sql .= "id = 0";
        $updatestatus = DB::select(DB::raw($sql));
        
        $query = "SELECT a.*, l.name AS lname, p.name AS pname, r.name AS rname FROM `units` AS a 
                    INNER JOIN `locations` AS l ON a.location = l.id
                    INNER JOIN `rooms` AS r ON a.room = r.id
                    INNER JOIN `positions` AS p ON a.position = p.id 
                    WHERE ";
        foreach($unit_ids as $id){
            $query .= "a.id = $id or ";
        }
        $query .= "a.id = 0";
        $units = DB::select(DB::raw($query));
        foreach($units as $unit){
            $unit->created_at = $this->datetimeformat($unit->created_at);
            $unit->updated_at = $this->datetimeformat($unit->updated_at);
        }
        $building = DB::select(DB::raw("SELECT buildingname FROM `buildings` WHERE id = $building_id"));
        $buildingname = $building[0]->buildingname;
        /* Print excel */
        $path = public_path()."/js/excel/template.xlsx";
        $excel = Excel::load($path);
        // return "bbb";
        // $excel = Excel::import($path);
        $excel->setActiveSheetIndex(0);

        $labelsheet = $excel->getSheet(0);
        $i = 3;
        foreach($units as $unit){
            $temp_w = explode(' ', $unit->width);
            if (isset($temp_w[1]))
            $temp_wdec = explode('/', $temp_w[1]);
            else
            $temp_wdec = array('0','1');
            $width = $this->dec2Frac((int)$temp_w[0] + (int)$temp_wdec[0]/(int)$temp_wdec[1]);
            
            $temp_h = explode(' ', $unit->height);
            if (isset($temp_h[1]))
            $temp_hdec = explode('/', $temp_h[1]);
            else
            $temp_hdec = array('0','1');
            $height = $this->dec2Frac((int)$temp_h[0] + (int)$temp_hdec[0]/(int)$temp_hdec[1]);
            
            $mod = fmod($i, 3);
            $dividevalue = floor($i/3);
            if ($mod == 0) $str = "A";
            else if ($mod == 1) $str = "B";
            else $str = "C";
            
            $value = $i - $mod - 2*$dividevalue;
            
            $labelsheet->SetCellValue($str.$value, $buildingname."\n# ".$unit->unit_num."\n".$unit->rname.", ".$unit->pname.", ".$unit->lname."\n".$width." x ".$height." - ".$unit->thickness." thick");
            $labelsheet->getStyle($str.$value)->getAlignment()->setWrapText(true);
            $i++;
        }
        
        $unitsheet = $excel->getSheet(1);
        $j = 5;
        $unitsheet->SetCellValue("D1", $buildingname);
        if (count($units) > 2)
        $unitsheet->insertNewRowBefore(6, count($units)-2);
        foreach($units as $unit){
            $unitsheet->SetCellValue("E".$j, $unit->unit_num);
            $unitsheet->SetCellValue("F".$j, $unit->width." x ".$unit->height);
            $j++;
        }
        
        $activesheet = $excel->getSheet(2);
        foreach($units as $unit){
            $lastindex = $excel->getSheetCount() - 1;
            $clonesheet = clone $activesheet;
            $clonesheet->setTitle("Unit ".$unit->unit_num);
            $excel->addSheet($clonesheet, $lastindex);
        }
        $index = 2;
        foreach($units as $unit){
            
            $temp_w = explode(' ', $unit->width);
            if (isset($temp_w[1]))
            $temp_wdec = explode('/', $temp_w[1]);
            else
            $temp_wdec = array('0','1');
            $width = (int)$temp_w[0] + (int)$temp_wdec[0]/(int)$temp_wdec[1];
            
            $temp_h = explode(' ', $unit->height);
            if (isset($temp_h[1]))
            $temp_hdec = explode('/', $temp_h[1]);
            else
            $temp_hdec = array('0','1');
            $height = (int)$temp_h[0] + (int)$temp_hdec[0]/(int)$temp_hdec[1];
            
            $spacerwidth = $this->dec2Frac($width - 0.875);
            $spacerheight = $this->dec2Frac($height - 0.875);
            
            $spacingwidth = $this->dec2Frac(($width - 0.875)/($unit->vertical_grids + 1));
            $spacingheight = $this->dec2Frac(($height - 0.875)/($unit->horizontal_girds + 1));
            $clonesheet = $excel->getSheet($index);
            $clonesheet->SetCellValue("G5",$buildingname);
            $clonesheet->SetCellValue("G9","Unit ".$unit->unit_num);
            $clonesheet->SetCellValue("G15", $spacerwidth);
            $clonesheet->SetCellValue("G17", $spacerheight);
            $clonesheet->SetCellValue("G21", $spacingwidth);
            $clonesheet->SetCellValue("G23", $spacingheight);
            $clonesheet->SetCellValue("F28", $unit->thickness.'"');
            $clonesheet->SetCellValue("J15", "- cut 2 spacer & ".$unit->horizontal_girds." grid to : ".$spacerwidth);
            $clonesheet->SetCellValue("J17", "- cut 2 spacers & ".$unit->vertical_grids." grid to : ".$spacerheight);
            $clonesheet->SetCellValue("J19", "- grab the spacers & grids that are ".$spacerwidth);
            $clonesheet->SetCellValue("K21", "- set the jig to ".$spacingwidth);
            $clonesheet->SetCellValue("K23", "- punch them ".$unit->vertical_grids." times");
            $clonesheet->SetCellValue("J25", "- grab the spacers & grids that are ".$spacerheight);
            $clonesheet->SetCellValue("K27", "- set the jig to ".$spacingheight);
            $clonesheet->SetCellValue("K29", "- punch them ".$unit->horizontal_girds." times");
            $clonesheet->SetCellValue("M33", $unit->width." x ".$unit->height);
            $index++;
        }
        // $activesheet->SetCellValue("A1","ffffff");
        $excel->removeSheetByIndex($lastindex+1);
        $fileName = 'Cut sheet ('.date("Y-m-d h-i-s").')';
        $file = public_path()."/excel"."/".$fileName.".xls";
        $excel->setFilename($fileName)->store('xls', public_path()."/excel");
        $headers = [
            'Content-Type' => 'application/vnd.ms-excel',
        ];
        return response()->download($file, $fileName, $headers);
        // return $file;
    }

    public function set_schedule(Request $request)
    {
        $scheduledate = $request->scheduledate;
        $unit_ids = $request->unit_ids;
        $sql = "UPDATE `units` SET STATUS = '4', schedule_for = '$scheduledate' WHERE ";
        foreach($unit_ids as $id){
            $sql .= "id = $id or ";
        }
        $sql .= "id = 0";
        $units = DB::select(DB::raw($sql));

        // $this->save_logs($unit_ids, 'updated status');
        return json_encode(1);
    }

    public function set_status(Request $request)
    {
        $res = new \stdClass;
        $status = $request->status;
        $unit_ids = $request->unit_ids;
        $sql = "UPDATE `units` SET STATUS = '$status' WHERE ";
        
        foreach($unit_ids as $id){
            $sql .= "id = $id or ";
        }
        $sql .= "id = 0";
        $units = DB::select(DB::raw($sql));

        $updated_units = DB::table('units')
                    ->whereIn('id', $unit_ids)
                    ->get();
        foreach($updated_units as $unit){
            $unit->created_at = $this->datetimeformat($unit->created_at);
            $unit->updated_at = $this->datetimeformat($unit->updated_at);
            if (isset($unit->schedule_for))
                $unit->schedule_for = $this->dateformat($unit->schedule_for);
        }
        $res->success = true;
        $res->data = $updated_units;
        return json_encode($res);
        // $this->save_logs($unit_ids, 'updated status');
        // return back()->withInfo('product successfully created!!');
    }

    public function get_activity_logs()
    {
        $res = new \stdClass;
        $logs = DB::table('activity_logs')->orderBy('created_at', 'desc')->get();
        $res->success = true;
        $res->data = $logs;
        return json_encode($res);
    }

    public function dec2Frac($decimal){
        $dec = $decimal - floor($decimal);
        if ($dec >= 0 && $dec < 0.03)
            $fracString = "";
        else if ($dec >= 0.03 && $dec < 0.0925)
            $fracString = "1/16";
        else if ($dec >= 0.0925 && $dec < 0.155)
            $fracString = "1/8";
        else if ($dec >= 0.155 && $dec < 0.2175)
            $fracString = "3/16";
        else if ($dec >= 0.2175 && $dec < 0.28)
            $fracString = "1/4";
        else if ($dec >= 0.28 && $dec < 0.3425)
            $fracString = "5/16";
        else if ($dec >= 0.3425 && $dec < 0.405)
            $fracString = "3/8";
        else if ($dec >= 0.405 && $dec < 0.4675)
            $fracString = "7/16";
        else if ($dec >= 0.4675 && $dec < 0.53)
            $fracString = "1/2";
        else if ($dec >= 0.53 && $dec < 0.5925)
            $fracString = "9/16";
        else if ($dec >= 0.5925 && $dec < 0.655)
            $fracString = "5/8";
        else if ($dec >= 0.655 && $dec < 0.7175)
            $fracString = "5/8";
        else if ($dec >= 0.7175 && $dec < 0.78)
            $fracString = "3/4";
        else if ($dec >= 0.78 && $dec < 0.8425)
            $fracString = "13/16";
        else if ($dec >= 0.8425 && $dec < 0.905)
            $fracString = "7/8";
        else if ($dec >= 0.905 && $dec < 0.9999)
            $fracString = "15/16";
        return floor($decimal)." ".$fracString;
    }

    public function save_logs($unit_ids, $action, $user){
        foreach($unit_ids as $unit_id){
            $unit = DB::select(DB::raw("SELECT u.*, b.`buildingname`, r.name AS roomname, p.name AS positionname, l.name AS locationname 
                                        FROM `units` AS u 
                                        INNER JOIN `buildings` AS b ON b.id = u.building 
                                        INNER JOIN `rooms` AS r ON r.id = u.`room`
                                        INNER JOIN `positions` AS p ON p.id = u.`position`
                                        INNER JOIN `locations` AS l ON l.id = u.`location` 
                                        WHERE u.id = $unit_id"));

            $log = New Activitylog;
            $log->date = date("m/d/y");
            $log->time = date("h:i:s a");
            $log->user = $user;
            $log->action = $action;
            $log->building = $unit[0]->buildingname;
            $log->entry = $unit[0]->unit_num."(".$unit[0]->roomname."/".$unit[0]->positionname."/".$unit[0]->locationname.")";
            $log->notes = $unit[0]->notes;
            $log->save();
        }
    }

    public function datetimeformat($datetime){
        $temp = explode(' ', date('Y-m-d h:i:s a', strtotime($datetime)));
        $date = $temp[0];
        $tempdate = explode('-', $date);
        $year = $tempdate[0];
        $month = $tempdate[1];
        $day = $tempdate[2];
        $temptime = array();
        $time = $temp[1];
        $temptime = explode(':', $time);
        $hour = $temptime[0];
        $minute = $temptime[1];
        $halfday = $temp[2];
        $date_time = $month."/".$day."/".$year." (".$hour.":".$minute.") ".$halfday;

        return $date_time;
    }

    public function dateformat($date){
        
        $tempdate = explode('-', $date);
        $year = $tempdate[0];
        $month = $tempdate[1];
        $day = $tempdate[2];
        $date_time = $month."/".$day."/".$year;

        return $date_time;
    }
}
