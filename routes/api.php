<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/sign_up', 'ApiController@sign_up');
Route::post('/sign_in', 'ApiController@sign_in');
Route::get('/get_rooms', 'ApiController@get_rooms');
Route::get('/get_buildings', 'ApiController@get_buildings');
Route::post('/add_unit', 'ApiController@add_unit');
Route::post('/update_unit', 'ApiController@update_unit');
Route::get('/get_units', 'ApiController@get_units');
Route::get('/get_scheduled_units', 'ApiController@get_scheduled_units');
Route::get('/cut_sheet', 'ApiController@cut_sheet');
Route::post('/set_schedule', 'ApiController@set_schedule');
Route::get('/get_activity_logs', 'ApiController@get_activity_logs');
Route::post('/set_status', 'ApiController@set_status');